package com.sidlors.microservices.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.sidlors.microservices.gateway.filters.PostFilter;
import com.sidlors.microservices.gateway.filters.PreFilter;
import com.sidlors.microservices.gateway.filters.PreRewriteFilter;
import com.sidlors.microservices.gateway.filters.RouteURLFilter;
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class ZuulGatewayServer {
    public static void main(String[] args) {
        SpringApplication.run(ZuulGatewayServer.class, args);
    }
    
    
    @Bean
    public RouteURLFilter routerFilter() {
        return new RouteURLFilter();
    }
    @Bean
    public PreFilter preFilter() {
        return new PreFilter();
    }
    @Bean
    public PreRewriteFilter preRewriteFilter() {
        return new PreRewriteFilter();
    }
    @Bean
    public PostFilter postFilter() {
        return new PostFilter();
    }
}
